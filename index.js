const fetch = require("node-fetch");

class MoneyLoverApiClient {
  constructor(authToken, refreshToken) {
    this._apiUrl = "https://web.moneylover.me/api";
    this._accessTokens = {
      access_token: authToken,
      refresh_token: refreshToken,
    };
  }

  async _doRequest(
    endpoint,
    dataObject,
    { authToken, clientId },
    tokenPrefix = "AuthJWT",
    customEndpoint
  ) {
    const headers = {};
    headers["Content-Type"] = "application/json;charset=UTF-8";
    if (authToken !== undefined)
      headers.authorization = `${tokenPrefix} ${authToken}`;
    if (clientId !== undefined) headers.client = clientId;

    const formatedEndpoint = (() => {
      if (customEndpoint === undefined) {
        return `${this._apiUrl}/${endpoint}`;
      } else {
        return customEndpoint;
      }
    })();

    // console.log(formatedEndpoint);
    // console.log(headers);
    // console.log(dataObject);

    return fetch(formatedEndpoint, {
      method: "POST",
      headers,
      body: JSON.stringify(dataObject),
    }).then((apiResponse) => {
      return apiResponse.json(); // parses JSON response into native JavaScript objects
    });
  }

  set accessTokens({ access_token, refresh_token }) {
    this._accessTokens = { access_token, refresh_token };
  }

  async _refreshToken(refreshToken) {
    return new Promise(async (resolve, reject) => {
      const dataObject = { refreshToken };
      const apiResponseData = await this._doRequest(
        "user/refresh-token",
        dataObject,
        {}
      ).then((apiResponse) => {
        return apiResponse.data;
      });

      if (apiResponseData === undefined) return reject(); // When token was not provided
      if (!apiResponseData.status)
        // When token was wrong
        return reject({
          code: apiResponseData.code,
          mesage: apiResponseData.message,
        });

      const { access_token, refresh_token } = apiResponseData;
      return resolve({ access_token, refresh_token });
    });
  }

  async _getAuthTokenAndClient() {
    const dataObject = {
      force: false,
      callback_url: "xD",
    };

    const apiResponseData = await this._doRequest(
      "user/login-url",
      dataObject,
      {}
    ).then((apiResponse) => {
      return apiResponse.data;
    });

    const { request_token, login_url } = apiResponseData;
    const urlParams = new URLSearchParams(login_url.split("?")[1]); // Get the data after '?' sign
    const client = urlParams.get("client"); // Parse the query parameters

    return { request_token, client };
  }

  async _getAccessTokens(clientId, requestToken, { login, password }) {
    const dataObject = { client_info: false, email: login, password };

    const apiResponseData = await this._doRequest(
      "user/login-url",
      dataObject,
      { clientId, authToken: requestToken },
      "Bearer",
      "https://oauth.moneylover.me/token"
    );

    return new Promise((resolve, reject) => {
      if (!apiResponseData.status)
        // When token was incorrect
        return reject({
          code: apiResponseData.code,
          mesage: apiResponseData.message,
        });

      const { access_token, refresh_token } = apiResponseData;
      return resolve({ access_token, refresh_token });
    });
  }

  async _loginToMl(login, password) {
    const authTokenAndClient = await this._getAuthTokenAndClient();

    try {
      const accessTokens = await this._getAccessTokens(
        authTokenAndClient.client,
        authTokenAndClient.request_token,
        { login, password }
      );

      return accessTokens;
    } catch (error) {
      throw error;
    }
  }

  async doLogin(login, password) {
    try {
      const accessTokens = await this._loginToMl(login, password);
      const { access_token, refresh_token } = accessTokens;

      this.accessTokens = { access_token, refresh_token };
      return this;
    } catch (error) {
      throw error;
    }
  }

  async doTokenRefresh() {
    try {
      const newAccessTokens = await this._refreshToken(
        this._accessTokens.refresh_token
      );
      const { access_token, refresh_token } = newAccessTokens;

      this.accessTokens = { access_token, refresh_token };
      return this;
    } catch (error) {
      throw error;
    }
  }

  async createTransaction(amount, category, displayDate, account, { note }) {
    const dataObject = {
      amount,
      category,
      displayDate,
      account,
      note,
    };

    return this._doRequest("transaction/add", dataObject, {
      authToken: this._accessTokens.access_token,
    }).then(async (apiResponse) => {
      if (apiResponse.error === 0) return apiResponse.data._id;

      try {
        if (this._accessTokens.refresh_token == undefined)
          throw "Refresh token can't be undefined";

        await this.doTokenRefresh();
      } catch (error) {
        console.error(new Error(JSON.stringify(error)));
        return null;
      }

      return this.createTransaction(amount, category, displayDate, account, {
        note,
      });
    });
  }

  async getTransationsByWallet(walletId, startDate, endDate) {
    const dataObject = {
      walletId,
      startDate,
      endDate,
    };

    return this._doRequest("transaction/list", dataObject, {
      authToken: this._accessTokens.access_token,
    }).then((apiResponse) => {
      // To to add error checking
      if (apiResponse.error !== 0) throw new Error(apiResponse);
      // return apiResponse;
      return apiResponse.data.transactions;
    });
  }

  async getUserWallets() {
    return this._doRequest(
      "wallet/list",
      {},
      {
        authToken: this._accessTokens.access_token,
      }
    ).then((apiResponse) => {
      // To to add error checking
      if (apiResponse.error !== 0) throw new Error(apiResponse);
      return apiResponse.data;
    });
  }
}

module.exports = { MoneyLoverApiClient };
