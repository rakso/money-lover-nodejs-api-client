const { MoneyLoverApiClient } = require("../index");
const { accessToken, walletId } = require("./config");

const mlClient = new MoneyLoverApiClient(accessToken);

mlClient
  .getTransationsByWallet(
    walletId,
    "2020-01-01T00:00:00+02:00",
    "2021-12-31T00:00:00+02:00"
  )
  .then((response) => {
    console.log(response);
    console.log();
    console.log(
      `${response.length} transaction${response.length > 1 ? "s" : ""}`
    );
  })
  .catch((error) => {
    console.error(error);
  });
