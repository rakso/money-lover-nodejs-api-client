const { MoneyLoverApiClient } = require("../index");

const mlClient = new MoneyLoverApiClient();

async function loginExample(login, pass) {
  const accessTokens = await mlClient._loginToMl(login, pass);
  console.log("Logged in");
  console.log(accessTokens);

  console.log("Refreshing tokens, because we can xD");
  const newAccessTokens = await mlClient._refreshToken(
    accessTokens.refresh_token
  );
  console.log("Tokens refreshed");
  console.log(newAccessTokens);
}

const LOGIN = "MYLOGIN";
const PASS = "MYPASS";

console.log("Logging to ML and then requesting token refresh");
loginExample(LOGIN, PASS);
