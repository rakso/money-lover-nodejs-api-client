const { MoneyLoverApiClient } = require("../index");

const mlClient = new MoneyLoverApiClient();

async function refreshTokens() {
  mlClient
    ._refreshToken(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoicmVmcmVzaC10b2tlbiIsImtleSI6ImQxNDU1MTc2LWNlNGUtNGM4YS04YjhjLTlmNzFmNDE1ODM4ZCIsImNsaWVudElkIjoiNWFjYWYzMDRhYTZjYzUwYzc3ZjdkMjI4IiwiY2xpZW50Ijoia0hpWmJGUU93NUxWIiwidXNlcklkIjoiNWYwNjQzMTI5ZjI0YmU2Nzk5MjgwOTc3IiwidG9rZW5EZXZpY2UiOiJjMWQ1ZmM0NC1lOGRkLTQ3ODctYTBkYi03YmUyMDY0YjQ4ODYiLCJzY29wZXMiOm51bGwsImlhdCI6MTYwMDI4MDc3MywiZXhwIjoxOTE1NjQwNzczfQ.VWyG6IdRcZjQfit2NE5ldyMjErVx5zsDX3gv20umN7s"
    )
    .then((accessTokens) => {
      console.log(accessTokens);
    })
    .catch((error) => {
      console.log(error);
    });
}

console.log("refresing tokens");
refreshTokens();
