const { MoneyLoverApiClient } = require("../index");

const mlClient = new MoneyLoverApiClient();
mlClient.accessTokens =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiYWNjZXNzLXRva2VuIiwidXNlcklkIjoiNWYwNjQzMTI5ZjI0YmU2Nzk5MjgwOTc3IiwidG9rZW5EZXZpY2UiOiJjNGQ1NWMzMS02OTdjLTRkYTMtOTNkOC05MmQ3NmYzZGMzZTkiLCJjbGllbnRJZCI6IjVhY2FmMzA0YWE2Y2M1MGM3N2Y3ZDIyOCIsImNsaWVudCI6ImtIaVpiRlFPdzVMViIsInNjb3BlcyI6bnVsbCwiaWF0IjoxNjAwMjkxMDU5LCJleHAiOjE2MDA4OTU4NTl9.i8jiHHNv7cjJJkxx-vouzpgKZtwanDYK08Y6kcFH-0c";

mlClient
  .createTransaction(
    123,
    "0185eb7905414db2adf2fc02ec4ceeed",
    "2020-09-20",
    "web954cb45a444369834e0b61981c7d2",
    {
      note: "#pasibrzuch_drugie xD",
    }
  )
  .then((response) => {
    console.log({ trid: response });
  })
  .catch((error) => {
    console.error(error);
  });
