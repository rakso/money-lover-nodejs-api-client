const { MoneyLoverApiClient } = require("../index");
const { accessToken } = require("./config");

const mlClient = new MoneyLoverApiClient(accessToken);

mlClient
  .getUserWallets()
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.error(error);
  });
