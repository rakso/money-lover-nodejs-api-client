const { MoneyLoverApiClient } = require("../index");

const mlClient = new MoneyLoverApiClient();

async function loginAndCreateTransaction(
  login,
  pass,
  amount,
  walletId,
  categoryId,
  date,
  note
) {
  console.log("Logging in");
  try {
    await mlClient.doLogin(login, pass);
  } catch (error) {
    console.error(error);
    return null;
  }

  console.log("Logged in, creating transaction");
  const trid = await mlClient
    .createTransaction(amount, categoryId, date, walletId, {
      note,
    })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.error(error);
    });

  return trid;
}

const LOGIN = null;
const PASS = null;

loginAndCreateTransaction(
  LOGIN,
  PASS,
  123,
  "web954cb45a444369834e0b61981c7d2",
  "0185eb7905414db2adf2fc02ec4ceeed",
  "2020-09-20",
  "xDD"
).then((trid) => {
  console.log({ trid });
});
